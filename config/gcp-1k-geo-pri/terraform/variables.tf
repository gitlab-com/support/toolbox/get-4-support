variable "project" {
  default = "<GET4S_PROJECT>"
}

variable "region" {
  default = "<GET4S_REGION>"
}

variable "zone" {
  default = "<GET4S_ZONE>"
}

variable "prefix" {
  default = "<GET4S_PREFIX>"
}

variable "external_ip" {
  default = "<GET4S_EXTERNAL_IP>"
}

variable "geo_site" {
  default = "<GET4S_GITLAB_GEO_SITE>"
}

variable "geo_deployment" {
  default = "<GET4S_GITLAB_GEO_DEPLOYMENT>"
}
