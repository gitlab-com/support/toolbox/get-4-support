terraform {
  backend "gcs" {
    bucket  = "<GET4S_PREFIX>-get-terraform-state"
  }
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 3"
    }
  }
}

provider "google" {
  project = var.project
  region  = var.region
  zone    = var.zone
}
