# README - GET configuration for `gcp-1k-geo`

## Pre-requisites
* In GCP console
    * [ ] Create Cloud Storage bucket <config_arch>-**pri**-<project-hash>-get-terraform-state e.g. **gcp-1k-geo**-pri-**project hash**-get-terraform-state e.g. gcp-1k-geo-pri-45b35c3k-get-terraform-state

      ```text
      Note: ensure that the bucket location is set to your region.
        * GCP: AMER, APAC, EMEA
      ```

    * [ ] Create Cloud Storage bucket <config_arch>-**sec**-<project-hash>-get-terraform-state e.g. **gcp-1k-geo**-sec-**project hash**-get-terraform-state e.g. gcp-1k-geo-sec-45b35c3k-get-terraform-state

    * [ ] Reserve a static address (VPC network > External IP) and name <config_arch> e.g. **gcp-1k-geo-pri**
 
      ```text
      Note: ensure that the region is set.
        * GCP: AMER: us-central1,   APAC: asia-southeast1,   EMEA: europe-west1
      ```
    
    * [ ] Reserve a static address (VPC network > External IP) and name <config_arch> e.g. **gcp-1k-geo-sec**

* In DNS (any DNS you control)
    * [ ] `A` record for `gcp-1k-geo-pri.<your-dns-domain>` to the static address
      * [ ] Confirm via `dig ...`!
    * [ ] `A` record for `gcp-1k-geo-sec.<your-dns-domain>` to the static address
      * [ ] Confirm via `dig ...`!

## gcp-1k-geo-pri
### Prepare the secrets
* Run `bash scripts/setup-secrets.sh <config_arch>`
```
bash scripts/setup-secrets.sh gcp-1k-geo-pri
```

* Edit files `secrets/<config_arch>/exports-gcp-1k-geo-pri-env`, `secrets/<config_arch>/exports-all-env`, 

```shell
vi secrets/gcp-1k-geo-pri/exports-gcp-1k-geo-pri-env  # Overrides specific to config_arch
vi secrets/gcp-1k-geo-pri/exports-all-env     # Set your common values here
```  

Two new environments variables have been added, these are used by GET to group the hosts which terraform creates
then to bring them together as a Geo installation..

> See [Advanced Terraform](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit/-/blob/master/docs/environment_advanced.md#terraform)
  * GET4S_GITLAB_GEO_SITE
    > `geo_site` - used to identify if a machine belongs to the primary or secondary site.
    This must be set to either `geo-primary-site` or `geo-secondary-site`.
  * GET4S_GITLAB_GEO_DEPLOYMENT 
    > `geo_deployment` - used to identify that a primary and secondary site belong to the same Geo deployment.
    This must be unique across all Geo deployments that will be stored alongside each other.

For this setup in `secrets/gcp-1k-geo-pri/exports-gcp-1k-geo-pri-env` we can use
  * GET4S_GITLAB_GEO_SITE="geo-primary-site" 
  * GET4S_GITLAB_GEO_DEPLOYMENT="gcp-1k-geo"
  * GET4S_GITLAB_EXTERNAL_URL="http[s]://gcp-1k-geo-pri.<your-dns-domain>" 

### Prepare the configuration files used by GET   
* Run `bash scripts/setup-arch.sh <config_arch>`
```shell
bash scripts/setup-arch.sh gcp-1k-geo-pri     # This will clone the specific GET tag
```

At this point

* secrets are stored in directory secrets/ which is ignored by Git
* these secrets have been used in the GET clone (also ignored by Git) `config/gcp-1k-geo-pri/gitlab-environment-toolkit`

and we can continue with the GET documentation for Terraform, Ansible provisioning steps.

* Terraform
```shell
cd config/gcp-1k-geo-pri/gitlab-environment-toolkit-gcp-1k-geo-pri/terraform/environments/gcp-1k-geo-pri
terraform init
terraform plan
terraform apply
```

* Ansible
```shell
cd config/gcp-1k-geo-pri/gitlab-environment-toolkit-gcp-1k-geo-pri/ansible
ansible all -m ping -i environments/gcp-1k-geo-pri/inventory --list-hosts
ansible-playbook -i environments/gcp-1k-geo-pri/inventory all.yml
```

Login to `https://gcp-1k-geo-pri.<you-dns-domain`!

## gcp-1k-geo-sec
### Prepare the secrets
* Run `bash scripts/setup-secrets.sh <config_arch>`
```
bash scripts/setup-secrets.sh gcp-1k-geo-sec
```

* Edit files `secrets/<config_arch>/exports-gcp-1k-geo-sec-env`, `secrets/<config_arch>/exports-all-env` 

```shell
vi secrets/gcp-1k-geo-sec/exports-gcp-1k-geo-sec-env  # Overrides specific to config_arch
vi secrets/gcp-1k-geo-sec/exports-all-env     # Set your common values here
```

For this setup in `secrets/gcp-1k-geo-sec/exports-gcp-1k-geo-sec-env` we can use
* GET4S_GITLAB_GEO_SITE="geo-secondary-site"
* GET4S_GITLAB_GEO_DEPLOYMENT="gcp-1k-geo"
* GET4S_GITLAB_EXTERNAL_URL="http[s]://gcp-1k-geo-sec.<your-dns-domain>"
 

### Prepare the configuration files used by GET
* Run `bash scripts/setup-arch.sh <config_arch>`
```shell
bash scripts/setup-arch.sh gcp-1k-geo-sec     # This will clone the specific GET tag
```

* Terraform
```shell
cd config/gcp-1k-geo-sec/gitlab-environment-toolkit-gcp-1k-geo-sec/terraform/environments/gcp-1k-geo-sec
terraform init
terraform plan
terraform apply
```

* Ansible
```shell
cd config/gcp-1k-geo-sec/gitlab-environment-toolkit-gcp-1k-geo-sec/ansible
ansible all -m ping -i environments/gcp-1k-geo-sec/inventory --list-hosts
ansible-playbook -i environments/gcp-1k-geo-sec/inventory all.yml
```

Login to `https://gcp-1k-geo-sec.<you-dns-domain`

## gcp-1k-geo
Wait until the both `gcp-1k-geo-pri` and `gcp-1k-geo-sec` configuration is completed.

### Prepare the secrets
* Run `bash scripts/setup-secrets.sh <config_arch>`
```
bash scripts/setup-secrets.sh gcp-1k-geo
```

* Edit files `secrets/<config_arch>/exports-gcp-1k-geo-env`, `secrets/<config_arch>/exports-all-env`

```shell
vi secrets/gcp-1k-geo/exports-gcp-1k-geo-env  # Overrides specific to config_arch
vi secrets/gcp-1k-geo/exports-all-env         # Set your common values here (Copy from gpc-1k-geo-pri!)
```  

Three main environment variables these control GEO Primary, Secondary assignments.

For this setup in `secrets/gcp-1k-geo/exports-gcp-1k-geo-env` we can use
* GET4S_GITLAB_EXTERNAL_URL="https://gcp-1k-geo-pri.<your-dns-domain>"
* GET4S_GITLAB_SECONDARY_EXTERNAL_URL="https://gcp-1k-geo-sec.<your-dns-domain>"
* GET4S_GITLAB_GEO_DEPLOYMENT="gcp-1k-geo"

### Prepare the configuration files used by GET
* Run `bash scripts/setup-arch.sh <config_arch>`
```shell
bash scripts/setup-arch.sh gcp-1k-geo     # This will clone the specific GET tag
```

* Ansible
```shell
cd config/gcp-1k-geo/gitlab-environment-toolkit-gcp-1k-geo/ansible
ansible all -m ping -i environments/gcp-1k-geo/inventory --list-hosts
ansible-playbook -i environments/gcp-1k-geo/inventory gitlab-geo.yml
```

We're now using `**gitlab-geo.yml**` instead of `**all.yml**` so that only the
configuration required to connect our Geo hosts is done.

Finally, login to `https://gcp-1k-geo-pri.<you-dns-domain`!
