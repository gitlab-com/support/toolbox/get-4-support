# README - GET configuration for `gcp-1k`

## Pre-requisites
* In GCP console
    * [ ] Create Cloud Storage bucket <config_arch>-<project-hash>-get-terraform-state e.g. **gcp-1k**-**project hash**-get-terraform-state e.g. gcp-1k-45b35c3k-get-terraform-state

      ```text
      Note: ensure that the bucket location is set to your region.
        * GCP: AMER, APAC, EMEA
      ```

    * [ ] Reserve a static address (VPC network > External IP) and name <config_arch> e.g. **gcp-1k**

      ```text
      Note: ensure that the region is set.
        * GCP: AMER: us-central1,   APAC: asia-southeast1,   EMEA: europe-west1
      ```

* In DNS (any DNS you control)
    * [ ] `A` record for `gcp-1k-<your-dns-domain)` to the static address
      * [ ] Confirm via `dig ...`!

## gcp-1k
### Prepare the secrets
* Run `bash scripts/setup-secrets.sh <config_arch>`
```
bash scripts/setup-secrets.sh gcp-1k
```

* Edit files `secrets/<config_arch>/exports-all-env`, `secrets/<config_arch>/exports-gcp-1k-env`

```shell
vi secrets/gcp-1k/exports-gcp-1k-env  # Overrides specific to config_arch
vi secrets/gcp-1k/exports-all-env     # Set your common values here
```  

### Prepare the configuration files used by GET
* Run `bash scripts/setup-arch.sh <config_arch>`
```shell
bash scripts/setup-arch.sh gcp-1k     # This will clone the specific GET tag
```

At this point

* secrets are stored in directory secrets/ which is ignored by Git
* these secrets have been used in the GET clone (also ignored by Git) `config/gcp-1k/gitlab-environment-toolkit`

and we can continue with the GET documentation for Terraform, Ansible provisioning steps.

* Terraform
```shell
cd config/gcp-1k/gitlab-environment-toolkit-gcp-1k/terraform/environments/gcp-1k
terraform init
terraform plan
terraform apply
```

* Ansible
```shell
cd config/gcp-1k/gitlab-environment-toolkit-gcp-1k/ansible
ansible all -m ping -i environments/gcp-1k/inventory --list-hosts
ansible-playbook -i environments/gcp-1k/inventory all.yml
```

Finally, login to `https://gcp-1k.<you-dns-domain`!
