# GET 4 Support - Quick Start

## Introduction

This is hoped to be an initial project where [GitLab Environment Toolkit](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit) configurations can be stored and deployed by GitLab Support Team members.

* A wrapper for GET
* Ability to use different versions of GET for each configuration
* Share Terraform, Ansible environment setups (no secrets in project git source)
* Quick for support to deploy a GET setup into a GitLab Sandbox

## Assumptions

* Using a GitLab Sandbox account - [Cloud Sandbox](https://gitlabsandbox.cloud/).
* Scope is restricted to provisioning an environment via GET to allow exploration of this GitLab installation setup.

## Related Projects

* [GitLab Environment Toolkit](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit)
* [Reference Architecture Tester](https://gitlab.com/gitlab-org/distribution/reference-architecture-tester)
* [Gitlab Environment Toolkit - Quality Config](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit-configs/quality) - QA examples
* [Gitlab Environment Toolkit - Geo Config](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit-configs/Geo) - QA Geo examples

## Pre-requisites
* If using OSx install `gsed`

```shell
brew install gsed
```

* Clone the GET project outside of this project and complete the following
sections

  * [GitLab Environment Toolkit - Preparing the environment](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit/-/blob/master/docs/environment_prep.md)
  * [GitLab Environment Toolkit - Provisioning the environment with Terraform](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit/-/blob/master/docs/environment_provision.md)
  * [GitLab Environment Toolkit - Configuring the environment with Ansible](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit/-/blob/master/docs/environment_configure.md)

to get all of the necessary tools installed along with any necessary manual steps to ensure authentication.  You can skip the the `terraform` and `ansible` commands as you will be using them later.

### GCP
In GCP here are some suggested variable to take note of assuming the use of a GitLab Sandbox.
* Project Name: <GCP Project Name> e.g. GitLabUsername-hash  (`username-45b35c3k`)
* Project id: <GCP Project ID> e.g. often the same name, expand the `select the project dialog in the GCP console to see the project` id
* Project Hash: <GCP Project name (hash component)> e.g. `45b35c3k`
    * we can use this as part of our Terraform prefix variable
* GCP Service Account Account Name: <GCP Service Account (sa_...)>  e.g. `gitlab-get`
    * in the GCP console extract the sa_<uniqueid> for the service account
    * Service Account Key File: e.g. `serviceaccount-gitlab-get.json`, this can be stored in `/Users/<username>/.ssh` and referenced later


### AWS
TBD

## Getting Started with a GET configuration
See the README in each config/<config_arch> directory e.g. [README-gcp-1k](config/gcp-1k/README-gcp-1k.md) config/gcp-1k for specific details

## Adding a new <config_arch>
Using a short name for the setup

* create directory structure under `config/`
* create templated versions of the Terraform/Ansible files
* create `config/<config_arch>/README-<config-arch>.md`
* create `config/<config_arch>/exports-gcp-1k-env.template` which contain specific details
* update `config/exports-all-env.template` (maybe new variables are required)


## Troubleshooting

### `bash scripts/setup-arch.sh <config-name>`

```
fatal: destination path 'config/gcp-1k-geo-pri/gitlab-environment-toolkit-gcp-1k-geo-pri' already exists and is not an empty directory.
```

You have previously configured this config.  Check if there are any active Cloud resources before removing the clone directory and running the `setup-arch.sh` again.

e.g.

```shell
cd 'config/<config-name>/gitlab-environment-toolkit-<config-name>'
terraform show
terraform destroy
```

If you don't want to `destroy` and provision new resources you have the option of updating the GET configuration files directly in
* `config/<config-name>/gitlab-environment-toolkit-<config-name>/terraform/environments/<config-name>`
* `config/<config-name>/gitlab-environment-toolkit-<config-name>/ansible/environments/<config-name>/inventory`
and run the `terrafrom`, `ansible` commands to activate.

### `terraform init`

```
Error: Failed to get existing workspaces: querying Cloud Storage failed: storage: bucket doesn't exist
```

This bucket needs to be created manually before running `terraform init`.

Check the name in `config/gcp-1k/gitlab-environment-toolkit/terraform/environment/gcp-1k/main.tf`

```shell
Error: "name" ("gcp-1k-geo-primary-80c27b0e-gitlab-rails-firewall-rule-http-https") doesn't match regexp "^(?:[a-z](?:[-a-z0-9]{0,61}[a-z0-9])?)$"

on ../../modules/gitlab_ref_arch_gcp/firewall.tf line 3, in resource "google_compute_firewall" "gitlab_http_https":
3:   name = "${var.prefix}-gitlab-rails-firewall-rule-http-https"
```

The prefix `gcp-1k-geo-primary-80c27b0e` is too long and not matching the regex.

```shell
Error: Error creating instance: googleapi: Error 400: Invalid value for field 'resource.labels': ''. Label value '<GET4S_GITLAB_GEO_SITE>-haproxy-external-primary' violates format constraints. The value can only contain lowercase letters, numeric characters, underscores and dashes. The value can be at most 63 characters long. International characters are allowed., invalid

  on ../../modules/gitlab_gcp_instance/main.tf line 6, in resource "google_compute_instance" "gitlab":
   6: resource "google_compute_instance" "gitlab" {
```

Check that environment variable `GET4S_GITLAB_GEO_SITE` has been set.
