#!/bin/bash -vx
# $1 reference architecture (name of the directory under config/)

# exit when any command fails
set -e

shopt -s extglob

if [[ $# -eq 0 ]]; then
  echo "Usage: setup-arch.sh <config-architecture>"
  echo "  config-architecture: Name of directory under config/ e.g. gcp-1k"
  exit 1
fi

CONFIG_ARCH="$1"
CONFIG_ARCH_DIR="config/$1"
GET_CLONE_DIR="${CONFIG_ARCH_DIR}/gitlab-environment-toolkit-${CONFIG_ARCH}"
GET_CLONE_TERRAFORM_DIR="${GET_CLONE_DIR}/terraform/environments/${CONFIG_ARCH}"
GET_CLONE_ANSIBLE_DIR="${GET_CLONE_DIR}/ansible/environments/${CONFIG_ARCH}/inventory"
SECRETS_DIR="secrets/$CONFIG_ARCH"
SECRETS_EXPORTS_ALL_ENV_FILE="${SECRETS_DIR}/exports-all-env"
SECRETS_EXPORTS_CONFIG_ARCH_FILE="${SECRETS_DIR}/exports-${CONFIG_ARCH}-env"



if [[ $OSTYPE == 'darwin'* ]]; then
  if ! type "gsed" > /dev/null; then
    echo "Please install gsed, 'brew install gsed'"  # to stop using BSD sed
    exit 1
  fi
  SED_IN_PLACE_CMD="gsed"
else
  SED_IN_PLACE_CMD="sed"
fi

#clone GET into the config-architecture to allow different versions of GET
if ! [[ -d "${CONFIG_ARCH_DIR}" ]]; then
  echo "Config directory for ${CONFIG_ARCH} does not exist"
  exit 1
fi

# source `secrets/config_arch` - both the export-all-env and exports-<config-arch>-env
# to allow for overrides
source "${SECRETS_EXPORTS_ALL_ENV_FILE}"
source "${SECRETS_EXPORTS_CONFIG_ARCH_FILE}"

git clone -b $GET4S_GET_CLONE_TAG $GET4S_GET_CLONE "${GET_CLONE_DIR}"

#
#prepare Terraform files
#
mkdir "${GET_CLONE_TERRAFORM_DIR}"
cp -Rv "${CONFIG_ARCH_DIR}"/terraform/*.tf "${GET_CLONE_TERRAFORM_DIR}"

#TODO Why are we using TF_VAR_, we can switch to GET4S_ ?
${SED_IN_PLACE_CMD} -i'' "s|<GET4S_PREFIX>|${TF_VAR_prefix}|g;
           s|<GET4S_PROJECT>|${TF_VAR_project}|g;
           s|<GET4S_REGION>|${TF_VAR_region}|g;
           s|<GET4S_ZONE>|${TF_VAR_zone}|g;
           s|<GET4S_EXTERNAL_IP>|${TF_VAR_external_ip}|g;
           s|<GET4S_GITLAB_GEO_SITE>|${GET4S_GITLAB_GEO_SITE}|g;
           s|<GET4S_GITLAB_GEO_DEPLOYMENT>|${GET4S_GITLAB_GEO_DEPLOYMENT}|g;" "${GET_CLONE_TERRAFORM_DIR}"/*.tf

#
# prepare Ansible files
#
mkdir -p "${GET_CLONE_ANSIBLE_DIR}"
cp -Rv "${CONFIG_ARCH_DIR}"/ansible/inventory/*.yml "${GET_CLONE_ANSIBLE_DIR}"

$SED_IN_PLACE_CMD -i'' "s|<GET4S_PREFIX>|${TF_VAR_prefix}|g;
           s|<GET4S_PROJECT>|${TF_VAR_project}|g;
           s|<GET4S_ZONE>|${TF_VAR_zone}|g;
           s|<ANSIBLE_USER>|${ANSIBLE_USER}|g;
           s|<ANSIBLE_SSH_PRIVATE_KEY_FILE>|${ANSIBLE_SSH_PRIVATE_KEY_FILE}|g;
           s|<GCP_SERVICE_ACCOUNT_HOST_FILE>|${GCP_SERVICE_ACCOUNT_HOST_FILE}|g;
           s|<GET4S_GITLAB_EXTERNAL_URL>|${GET4S_GITLAB_EXTERNAL_URL}|g;
           s|<GET4S_GITLAB_SECONDARY_EXTERNAL_URL>|${GET4S_GITLAB_SECONDARY_EXTERNAL_URL}|g;
           s|<GET4S_GITLAB_GEO_DEPLOYMENT>|${GET4S_GITLAB_GEO_DEPLOYMENT}|g;
           s|<GET4S_GITLAB_LICENSE_FILE>|${GET4S_GITLAB_LICENSE_FILE}|g;
           s|<GET4S_GITLAB_REPO_PACKAGE>|${GET4S_GITLAB_REPO_PACKAGE}|g;
           s|<GET4S_GITLAB_REPO_SCRIPT_URL>|${GET4S_GITLAB_REPO_SCRIPT_URL}|g;
           s|<GET4S_EXTERNAL_SSL_SOURCE>|${GET4S_EXTERNAL_SSL_SOURCE}|g;
           s|<GET4S_EXTERNAL_SSL_LETSENCRYPT_ISSUER_EMAIL>|${GET4S_EXTERNAL_SSL_LETSENCRYPT_ISSUER_EMAIL}|g;
           s|<GET4S_EXTERNAL_SSL_FILES_HOST_CERTIFICATE_FILE>|${GET4S_EXTERNAL_SSL_FILES_HOST_CERTIFICATE_FILE}|g;
           s|<GET4S_EXTERNAL_SSL_FILES_HOST_KEY_FILE>|${GET4S_EXTERNAL_SSL_FILES_HOST_KEY_FILE}|g;
           s|<GET4S_GITLAB_ROOT_PASSWORD>|${GET4S_GITLAB_ROOT_PASSWORD}|g;
           s|<GET4S_GITLAB_GRAFANA_PASSWORD>|${GET4S_GITLAB_GRAFANA_PASSWORD}|g;
           s|<GET4S_GITLAB_POSTGRES_PASSWORD>|${GET4S_GITLAB_POSTGRES_PASSWORD}|g;
           s|<GET4S_GITLAB_CONSUL_DATABASE_PASSWORD>|${GET4S_GITLAB_CONSUL_DATABASE_PASSWORD}|g;
           s|<GET4S_GITLAB_GITALY_TOKEN>|${GET4S_GITLAB_GITALY_TOKEN}|g;
           s|<GET4S_GITLAB_PGBOUNCER_PASSWORD>|${GET4S_GITLAB_PGBOUNCER_PASSWORD}|g;
           s|<GET4S_GITLAB_REDIS_PASSWORD>|${GET4S_GITLAB_REDIS_PASSWORD}|g;
           s|<GET4S_GITLAB_PRAEFECT_POSTGRES_PASSWORD>|${GET4S_GITLAB_PRAEFECT_POSTGRES_PASSWORD}|g;
           s|<GET4S_GITLAB_PRAEFECT_EXTERNAL_TOKEN>|${GET4S_GITLAB_PRAEFECT_EXTERNAL_TOKEN}|g;
           s|<GET4S_GITLAB_PRAEFECT_INTERNAL_TOKEN>|${GET4S_GITLAB_PRAEFECT_INTERNAL_TOKEN}|g;" "${GET_CLONE_ANSIBLE_DIR}"/*.yml
