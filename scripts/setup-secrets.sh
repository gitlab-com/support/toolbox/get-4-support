#!/bin/bash
#

# exit when any command fails
set -e

shopt -s extglob

if [[ $# -eq 0 ]]; then
  echo "Usage: setup-secrets.sh <config-architecture>"
  echo "  config-architecture: Name of directory under config/ e.g. gcp-1k"
  exit 1
fi

CONFIG_ARCH="$1"
SECRETS_DIR="secrets/$CONFIG_ARCH"
SECRETS_ALL_ENV_FILE="exports-all-env"
SECRETS_CONFIG_ARCH_ENV_FILE="exports-${CONFIG_ARCH}-env"


if ! [[ -d $SECRETS_DIR ]]; then
  mkdir -p $SECRETS_DIR
else
  if [[ -f $SECRETS_DIR/${SECRETS_ALL_ENV_FILE} ]]; then
    cp -p $SECRETS_DIR/${SECRETS_ALL_ENV_FILE} $SECRETS_DIR/${SECRETS_ALL_ENV_FILE}-$(date +%s).bak
  fi
  if [[ -f "$SECRETS_DIR/${SECRETS_CONFIG_ARCH_ENV_FILE}" ]]; then
    cp -p "$SECRETS_DIR/${SECRETS_CONFIG_ARCH_ENV_FILE}" "$SECRETS_DIR/${SECRETS_CONFIG_ARCH_ENV_FILE}"-$(date +%s).bak
  fi
fi

#delete the template warnings
sed "/^#TEMPLATE-WARNING/d" config/${SECRETS_ALL_ENV_FILE}.template 1>"$SECRETS_DIR/${SECRETS_ALL_ENV_FILE}"
sed "/^#TEMPLATE-WARNING/d" "config/${CONFIG_ARCH}/${SECRETS_CONFIG_ARCH_ENV_FILE}.template" 1>"$SECRETS_DIR/${SECRETS_CONFIG_ARCH_ENV_FILE}"

echo
echo "Update files "
echo "  $SECRETS_DIR/${SECRETS_CONFIG_ARCH_ENV_FILE}"
echo "  $SECRETS_DIR/${SECRETS_ALL_ENV_FILE}"
echo "for your configuration."
echo
echo "Note: The directory '$SECRETS_DIR' is ignored by .gitignore"



